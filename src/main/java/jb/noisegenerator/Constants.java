package jb.noisegenerator;

public class Constants {
	public static double TAU = 2 * Math.PI;
	public static int SAMPLE_RATE = 44100;
}
