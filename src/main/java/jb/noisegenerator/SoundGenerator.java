package jb.noisegenerator;

import jb.noisegenerator.instrument.Instrument;

import javax.sound.sampled.SourceDataLine;
import java.nio.ByteBuffer;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class SoundGenerator {
	private List<Instrument> instruments = new ArrayList<>();
	private int channelsCount;
	private int resolution;

	private int lastNotePlayedIndex;
	private int sampleCount;
	private int byteCount;
	private int bufferSizeInFrames;
	private int bufferSizeInBytes;
	private int frameSize;
	private int framesCount;
	private int frameIndex;

	public SoundGenerator() {
		channelsCount = 2;
		resolution = 4;
		frameIndex = 0;
		byteCount = sampleCount * resolution;
		bufferSizeInFrames = 32;
		bufferSizeInBytes = bufferSizeInFrames * channelsCount * resolution;
	}

	public void addInstrument(Instrument instrument) {
		instruments.add(instrument);
	}

	public void removeInstrument(Instrument instrument) {
		instruments.remove(instrument);
	}

	public void playSong(Duration length, SourceDataLine line) {
		frameIndex = 0;
		framesCount = (int) (length.getSeconds() * Constants.SAMPLE_RATE);
		lastNotePlayedIndex = framesCount - SoundMaths.secondsToFrames(10);
		sampleCount = framesCount * channelsCount;
		byteCount = sampleCount * resolution;
		while (frameIndex < framesCount) {
			ByteBuffer buffer = playBuffer();
			line.write(buffer.array(), 0, bufferSizeInBytes);
		}
	}

	ByteBuffer playBuffer() {
		ByteBuffer buffer = ByteBuffer.allocate(bufferSizeInBytes);
		for (int bufferIndex = 0; bufferIndex < bufferSizeInFrames && frameIndex < framesCount; bufferIndex++) {
			if (frameIndex == lastNotePlayedIndex) {
				for (Instrument instrument : instruments) {
					instrument.stop();
				}
			}
			Frame finalFrame = new Frame(0, 0);
			for (Instrument instrument : instruments) {
				Frame frame = instrument.getFrame();
				finalFrame.add(frame);
			}
			buffer.putInt(finalFrame.getLeftChannel());
			buffer.putInt(finalFrame.getRightChannel());
			frameIndex++;
		}
		return buffer;
	}
}
