package jb.noisegenerator;

import com.google.common.collect.Range;
import com.google.common.math.DoubleMath;

public class Panning {
	private double leftVolume = 0.5;
	private double leftVolumeTarget = 0.5;
	private double rightVolume = 0.5;
	private double changePerSecond = 0.1;
	private int sampleRate = 44100;
	private boolean changing = false;
	private Range<Double> volumeRange = Range.closed(0.0, 1.0);

	public void update() {
		if (changing) {
			double volumeChange = changePerSecond / sampleRate;
			if (leftVolumeTarget > leftVolume) {
				leftVolume += volumeChange;
				rightVolume -= volumeChange;
				if (leftVolume > leftVolumeTarget) {
					leftVolume = leftVolumeTarget;
					changing = false;
				}
			} else {
				leftVolume -= volumeChange;
				rightVolume += volumeChange;
				if (leftVolume < leftVolumeTarget) {
					leftVolume = leftVolumeTarget;
					changing = false;
				}
			}
		}
	}

	public double getLeftVolume() {
		return leftVolume;
	}

	public void setLeftVolumeTarget(double leftVolumeTarget) {
		assert(volumeRange.contains(leftVolumeTarget));
		if (DoubleMath.fuzzyEquals(leftVolume, leftVolumeTarget, 0.001)) {
			return;
		}
		changing = true;
		this.leftVolumeTarget = leftVolumeTarget;
	}

	public double getRightVolume() {
		return rightVolume;
	}

	public boolean isChanging() {
		return changing;
	}

	public double getChangePerSecond() {
		return changePerSecond;
	}

	public void setChangePerSecond(double changePerSecond) {
		this.changePerSecond = changePerSecond;
	}
}
