package jb.noisegenerator;

public class SoundMaths {
	public static final int frameRate = 44100;

	public static int secondsToFrames(double seconds) {
		return (int) (seconds * frameRate);
	}

	/**
	 * Multiply a int with a double while allowing overflow
	 * (using normal multiplication, an overflow in this case
	 * would return Integer.MAX_VALUE)
	 */
	public static double multiplyWithOverflow(int a, double multiplier) {
		// Java doesn't allow overflow of doubles (and floats), but does allow it
		// for ints (and chars and shorts), so we have to convert multiplication
		// into additions to allow overflow
		double result = a * multiplier;
		if (isInIntegerRange(result)) {
			return result;
		} else {
			// multiplying 2 ints can overflow
			// so we multiply with the whole part of the multiplier
			int multiplierWhole = 0;
			if (multiplier > 0) {
				multiplierWhole = (int) Math.floor(multiplier);
			} else {
				multiplierWhole = (int) Math.ceil(multiplier);
			}
			int intResult = multiplierWhole * a;

			// get the decimal part of the multiplier
			// we know that it is between -1.0 and 1.0,
			// so multiplying it with an int can't overflow
			double multiplierRest = 0;
			if (multiplier > 0) {
				multiplierRest = multiplier - multiplierWhole;
			} else {
				multiplierRest = multiplier + multiplierWhole;
			}
			double rest = a * multiplierRest;

			double doubleResult = 0;

			if (result > 0) {
				doubleResult = (double) intResult + rest;
				if (isInIntegerRange(doubleResult)) {
					return doubleResult;
				} else {
					int untilOverflow = Integer.MAX_VALUE - intResult;
					double addAfterOverflow = rest - untilOverflow;
					doubleResult = Integer.MIN_VALUE + addAfterOverflow;
				}
			} else {
				doubleResult = (double) intResult + rest;
				if (isInIntegerRange(doubleResult)) {
					return doubleResult;
				} else {
					int untilUnderflow = Math.abs(Integer.MIN_VALUE) - (Math.abs(intResult));
					double substractAfterUnderflow = Math.abs(rest) - untilUnderflow;
					doubleResult = Integer.MAX_VALUE - substractAfterUnderflow;
				}
			}
			return doubleResult;
		}
	}

	public static boolean isInIntegerRange(double a) {
		return a < Integer.MAX_VALUE && a > Integer.MIN_VALUE;
	}
}
