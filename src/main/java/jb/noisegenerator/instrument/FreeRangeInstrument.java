package jb.noisegenerator.instrument;

import jb.noisegenerator.Constants;
import jb.noisegenerator.Frame;
import jb.noisegenerator.note.RandomNote;
import jb.noisegenerator.SoundMaths;
import jb.noisegenerator.note.ConstantNote;
import jb.noisegenerator.note.Note;

import java.util.Random;

/**
 * An instrument that can play any frequency between a max and min value
 */
public class FreeRangeInstrument extends Instrument {
	private PanningManager panningManager;
	private VolumeManager volumeFirstPassManager;
	private VolumeManager volumeSecondPassManager;
	private MinMaxFrequencyManager minMaxFrequencyManager;
	private SilenceManager silenceManager;
	private int nextNoteFrame;
	private Random randomGenerator;
	private Note currentNote;
	private AverageNoteLengthManager averageNoteLengthManager;
	private TimeBetweenNotesManager timeBetweenNotesManager;
	private PhaseManager phaseManager;
	private FrequencyTargetManager frequencyTargetManager;
	private NormalDistributionFrequencyManager normalDistributionFrequencyManager;
	private int reseedTimer = 0;

	public FreeRangeInstrument(long seed) {
		super();
		randomGenerator = new Random(seed);
		this.panningManager = new PanningManager(randomGenerator, 1.0);
		volumeFirstPassManager = new VolumeManager(
				randomGenerator,
				1.0,
				1,
				5.0,
				0.00001
		);
		volumeSecondPassManager = new VolumeManager(
				randomGenerator,
				1.0,
				0.2,
				1,
				0.00001
		);
		averageNoteLengthManager = new AverageNoteLengthManager(
				randomGenerator,
				1.0,
				randomGenerator.nextDouble() * 5
		);
		minMaxFrequencyManager = new MinMaxFrequencyManager(
				randomGenerator,
				1.0,
				1,
				100,
				4000,
				1000
		);
		timeBetweenNotesManager = new TimeBetweenNotesManager(randomGenerator, 1.0, 0.1);
		silenceManager = new SilenceManager(randomGenerator, 2.0);
		phaseManager = new PhaseManager(randomGenerator, 15.0);
		phaseManager.setChangeListener(this::newPhase);
		normalDistributionFrequencyManager = new NormalDistributionFrequencyManager(
				randomGenerator,
				5.0,
				500,
				500
		);
		frequencyTargetManager = new FrequencyTargetManager(
				randomGenerator,
				2.0,
				this::updateFrequency
		);
	}

	private void newPhase() {
		volumeFirstPassManager.setAverageInterval(randomGenerator.nextDouble() * 5);
		volumeFirstPassManager.setMinVolume(randomGenerator.nextDouble() * 2);
		volumeFirstPassManager.setMaximumVolume(volumeFirstPassManager.getMinVolume() + randomGenerator.nextDouble() * 5);
		volumeFirstPassManager.setChangeSpeedMultiplier(randomGenerator.nextDouble() * 0.0001);

		volumeSecondPassManager.setAverageInterval(randomGenerator.nextDouble() * 5);
		volumeSecondPassManager.setMinVolume(randomGenerator.nextDouble() * 0.1);
		volumeSecondPassManager.setMaximumVolume(1.0);
		volumeSecondPassManager.setChangeSpeedMultiplier(randomGenerator.nextDouble() * 0.0001);
		averageNoteLengthManager.setAverageInterval(randomGenerator.nextDouble() * 5);
		averageNoteLengthManager.setAverageNoteLength(randomGenerator.nextDouble() * 3);
		normalDistributionFrequencyManager.setAverageMean(randomGenerator.nextDouble() * 5000);
		normalDistributionFrequencyManager.setAverageStandardDeviation(randomGenerator.nextDouble() * 2500);
		timeBetweenNotesManager.setAverageInterval(randomGenerator.nextDouble() * 5);
		timeBetweenNotesManager.setAverage(randomGenerator.nextDouble() / 4);
		silenceManager.setAverageInterval(randomGenerator.nextDouble() * 10);

		frequencyTargetManager.setAverageInterval(randomGenerator.nextDouble() * 3);
	}

	@Override
	public Frame getFrame() {
		if (reseedTimer > SoundMaths.secondsToFrames(15.0)) {
			randomGenerator.setSeed(randomGenerator.nextInt());
			reseedTimer = 0;
		}
		reseedTimer++;
		silenceManager.update();
		phaseManager.update();
		panningManager.update();
		averageNoteLengthManager.update();
		minMaxFrequencyManager.update();
		timeBetweenNotesManager.update();
		if (currentNote != null) {
			if (currentNote.isExpired()) {
				currentNote = null;
			}
		}
		if (currentNote == null && canPlayNewNotes && !silenceManager.isSilent()) {
			if (frameIndex >= nextNoteFrame) {
				double noteDuration = randomGenerator.nextDouble() * averageNoteLengthManager.getAverageNoteLength();
				double attackDuration = noteDuration * randomGenerator.nextDouble();
				double timeLeftInNote = noteDuration - attackDuration;
				double decayDuration = timeLeftInNote * randomGenerator.nextDouble();
				if (randomGenerator.nextInt(100) == 0) {
					currentNote = new ConstantNote(noteDuration, randomGenerator.nextInt(Integer.MAX_VALUE));
				} else {
					currentNote = new RandomNote(
							noteDuration,
							attackDuration,
							decayDuration,
							randomGenerator,
							normalDistributionFrequencyManager.getNewFrequency()
					);
				}
				int maxTimeBeforeNextNote = SoundMaths.secondsToFrames(timeBetweenNotesManager.getAverageTimeBetweenNotes()) * 2;
				int timeBeforeNextNote = randomGenerator.nextInt(maxTimeBeforeNextNote);
				nextNoteFrame = frameIndex + SoundMaths.secondsToFrames(noteDuration) + timeBeforeNextNote + 1;
			}
		}
		if (currentNote != null) {
			volumeFirstPassManager.update();
			volumeSecondPassManager.update();
			normalDistributionFrequencyManager.update();
			frequencyTargetManager.update();
			Frame frame = currentNote.getFrame();
			frame.applyPanning(panningManager.getPanning());
			frame.applyVolume(volumeFirstPassManager.getGlobalVolume());
			frame.applyVolume(volumeSecondPassManager.getGlobalVolume());
			frameIndex++;
			return frame;
		} else {
			frameIndex++;
			return new Frame();
		}
	}

	private void updateFrequency() {
		if (currentNote != null) {
			currentNote.setTargetFrequency(normalDistributionFrequencyManager.getNewFrequency());
		}
	}
}
