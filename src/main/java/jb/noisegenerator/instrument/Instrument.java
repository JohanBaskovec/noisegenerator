package jb.noisegenerator.instrument;

import jb.noisegenerator.Frame;

public abstract class Instrument {
	protected int frameIndex;
	protected boolean canPlayNewNotes;

	public Instrument() {
		canPlayNewNotes = true;
	}

	public abstract Frame getFrame();

	public void stop() {
		canPlayNewNotes = false;
	}
}
