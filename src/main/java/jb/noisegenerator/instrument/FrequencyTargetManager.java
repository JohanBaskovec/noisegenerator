package jb.noisegenerator.instrument;

import java.util.Random;

public class FrequencyTargetManager extends ChangingDataManager {
	private Runnable changeListener;

	public FrequencyTargetManager(Random randomGenerator, double averageInterval, Runnable changeListener) {
		super(randomGenerator, averageInterval);
		this.changeListener = changeListener;
	}

	@Override
	void onChange() {
		this.changeListener.run();
	}
}
