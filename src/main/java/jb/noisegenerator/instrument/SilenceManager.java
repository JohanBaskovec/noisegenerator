package jb.noisegenerator.instrument;

import java.util.Random;

public class SilenceManager extends ChangingDataManager {
	private boolean silent = false;

	public SilenceManager(Random randomGenerator, double averageInterval) {
		super(randomGenerator, averageInterval);
	}

	@Override
	void onChange() {
		silent = !silent;
	}

	public boolean isSilent() {
		return silent;
	}
}
