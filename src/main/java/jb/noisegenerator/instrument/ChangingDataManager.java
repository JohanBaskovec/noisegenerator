package jb.noisegenerator.instrument;

import jb.noisegenerator.SoundMaths;

import java.util.Random;

public abstract class ChangingDataManager {
	protected int nextChangeTime;
	protected int timer;
	protected double averageInterval;
	protected int averageIntervalSample;
	protected Random randomGenerator;

	public ChangingDataManager(Random randomGenerator, double averageInterval) {
		this.randomGenerator = randomGenerator;
		setAverageInterval(averageInterval);
	}

	public void update() {
		if (timer >= nextChangeTime) {
			onChange();
			nextChangeTime = timer + randomGenerator.nextInt(averageIntervalSample * 2);
		}
		timer++;
	}

	abstract void onChange();

	public double getAverageInterval() {
		return averageInterval;
	}

	public void setAverageInterval(double averageInterval) {
		this.averageInterval = averageInterval;
		averageIntervalSample = SoundMaths.secondsToFrames(averageInterval);
	}

}
