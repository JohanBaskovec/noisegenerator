package jb.noisegenerator.instrument;

import jb.noisegenerator.Panning;

import java.util.Random;

public class PanningManager extends ChangingDataManager {
	private final Panning panning = new Panning();

	public PanningManager(Random randomGenerator, double averageInterval) {
		super(randomGenerator, averageInterval);
	}

	@Override
	void onChange() {
		if (!panning.isChanging()) {
			panning.setLeftVolumeTarget(randomGenerator.nextDouble());
			panning.setChangePerSecond(randomGenerator.nextDouble());
		}
	}

	@Override
	public void update() {
		panning.update();
		super.update();
	}

	public Panning getPanning() {
		return panning;
	}
}
