package jb.noisegenerator.instrument;

import java.util.Random;

public class VolumeManager extends ChangingDataManager {
	private double minVolume;
	private double maximumVolume;
	private double changeSpeedMultiplier;
	private int nextVolumeChange = 0;
	private double volumeTarget = 1.0;
	private double globalVolume = 1.0;
	// percentage of change per frame
	private double changeSpeed = 0.001;

	public VolumeManager(
			Random randomGenerator,
			double averageInterval,
			double minimumVolume,
			double maximumVolume,
			double changeSpeedMultiplier
	) {
		super(randomGenerator, averageInterval);
		this.minVolume = minimumVolume;
		this.maximumVolume = maximumVolume;
		this.changeSpeedMultiplier = changeSpeedMultiplier;
	}

	@Override
	void onChange() {
		volumeTarget = randomGenerator.nextDouble() * (maximumVolume - minVolume) + minVolume;
		changeSpeed = randomGenerator.nextDouble() * changeSpeedMultiplier;
	}

	@Override
	public void update() {
		if (volumeTarget > globalVolume) {
			globalVolume += changeSpeed;
			if (volumeTarget < globalVolume) {
				globalVolume = volumeTarget;
			}
		} else if (volumeTarget < globalVolume) {
			globalVolume -= changeSpeed;
			if (volumeTarget > globalVolume) {
				globalVolume = volumeTarget;
			}
		}
		super.update();
	}

	public double getGlobalVolume() {
		return globalVolume;
	}

	public double getMinVolume() {
		return minVolume;
	}

	public void setMinVolume(double minVolume) {
		this.minVolume = minVolume;
	}

	public double getMaximumVolume() {
		return maximumVolume;
	}

	public void setMaximumVolume(double maximumVolume) {
		this.maximumVolume = maximumVolume;
	}

	public double getChangeSpeedMultiplier() {
		return changeSpeedMultiplier;
	}

	public void setChangeSpeedMultiplier(double changeSpeedMultiplier) {
		this.changeSpeedMultiplier = changeSpeedMultiplier;
	}
}
