package jb.noisegenerator.instrument;

import java.util.Random;

public class VolumeSecondPassManager extends ChangingDataManager {
	private int nextVolumeChange = 0;
	private double volumeTarget = 1.0;
	private double globalVolume = 1.0;
	// percentage of change per frame
	private double changeSpeed = 0.001;

	public VolumeSecondPassManager(Random randomGenerator, double averageInterval) {
		super(randomGenerator, averageInterval);
	}

	@Override
	void onChange() {
		volumeTarget = randomGenerator.nextDouble() * 5;
		changeSpeed = randomGenerator.nextDouble() / 10;
	}

	@Override
	public void update() {
		if (volumeTarget > globalVolume) {
			globalVolume += changeSpeed;
			if (volumeTarget < globalVolume) {
				globalVolume = volumeTarget;
			}
		} else if (volumeTarget < globalVolume) {
			globalVolume -= changeSpeed;
			if (volumeTarget > globalVolume) {
				globalVolume = volumeTarget;
			}
		}
		super.update();
	}

	public int getNextVolumeChange() {
		return nextVolumeChange;
	}

	public double getVolumeTarget() {
		return volumeTarget;
	}

	public double getGlobalVolume() {
		return globalVolume;
	}
}
