package jb.noisegenerator.instrument;

import java.util.Random;

public class AverageNoteLengthManager extends ChangingDataManager {
	private final double averageValue;
	private double averageNoteLength;

	public AverageNoteLengthManager(Random randomGenerator, double averageInterval, double averageValue) {
		super(randomGenerator, averageInterval);
		this.averageValue = averageValue;
	}

	@Override
	void onChange() {
		averageNoteLength = randomGenerator.nextDouble() * (averageValue * 2) + 0.1;
	}

	public double getAverageNoteLength() {
		return averageNoteLength;
	}

	public void setAverageNoteLength(double averageNoteLength) {
		this.averageNoteLength = averageNoteLength;
	}
}
