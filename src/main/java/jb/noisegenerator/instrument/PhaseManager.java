package jb.noisegenerator.instrument;

import java.util.Random;

public class PhaseManager extends ChangingDataManager {
	private Runnable newPhaseListener;

	public PhaseManager(Random randomGenerator, double averageInterval) {
		super(randomGenerator, averageInterval);
	}

	public void setChangeListener(Runnable newPhase) {
		this.newPhaseListener = newPhase;
	}

	@Override
	void onChange() {
		newPhaseListener.run();
	}
}
