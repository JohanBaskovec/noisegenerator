package jb.noisegenerator.instrument;

import java.util.Random;

public class NormalDistributionFrequencyManager extends ChangingDataManager {
	private double averageStandardDeviation;
	private double averageMean;
	private double mean;
	private double standardDeviation;
	private double frequency;

	public NormalDistributionFrequencyManager(
			Random randomGenerator,
			double averageInterval,
			double averageMean,
			double averageStandardDeviation
	) {
		super(randomGenerator, averageInterval);
		this.averageMean = averageMean;
		this.averageStandardDeviation = averageStandardDeviation;
	}

	@Override
	void onChange() {
		mean = randomGenerator.nextDouble() * averageMean * 2;
		standardDeviation = randomGenerator.nextDouble() * averageStandardDeviation * 2;
	}

	public double getNewFrequency() {
		double gaussian = randomGenerator.nextGaussian();
		double frequency =  gaussian * standardDeviation + mean;
		if (frequency < 0) {
			frequency = Math.abs(frequency);
		}
		return frequency;
	}

	public double getAverageStandardDeviation() {
		return averageStandardDeviation;
	}

	public void setAverageStandardDeviation(double averageStandardDeviation) {
		if (averageStandardDeviation > mean) {
			averageStandardDeviation = mean + 1;
		}
		this.averageStandardDeviation = averageStandardDeviation;
	}

	public double getAverageMean() {
		return averageMean;
	}

	public void setAverageMean(double averageMean) {
		this.averageMean = averageMean;
	}
}
