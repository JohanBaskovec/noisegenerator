package jb.noisegenerator.instrument;

import java.util.Random;

public class MinMaxFrequencyManager extends ChangingDataManager {
	private int minMin;
	private int maxMin;
	private int maxMax;
	private int minMax;
	private double minFrequency;
	private double maxFrequency;

	public MinMaxFrequencyManager(
			Random randomGenerator,
			double averageInterval,
			int minMin,
			int maxMin,
			int maxMax,
			int minMax
	) {
		super(randomGenerator, averageInterval);
		this.minMin = minMin;
		this.maxMin = maxMin;
		this.maxMax = maxMax;
		this.minMax = minMax;
	}

	@Override
	void onChange() {
		minFrequency = randomGenerator.nextInt(maxMin) + minMin;
		maxFrequency = randomGenerator.nextInt(maxMax) + minMax;
	}

	public double getMinFrequency() {
		return minFrequency;
	}

	public void setMinFrequency(double minFrequency) {
		this.minFrequency = minFrequency;
	}

	public double getMaxFrequency() {
		return maxFrequency;
	}

	public void setMaxFrequency(double maxFrequency) {
		this.maxFrequency = maxFrequency;
	}

	public int getMinMin() {
		return minMin;
	}

	public void setMinMin(int minMin) {
		this.minMin = minMin;
	}

	public int getMaxMin() {
		return maxMin;
	}

	public void setMaxMin(int maxMin) {
		this.maxMin = maxMin;
	}

	public int getMaxMax() {
		return maxMax;
	}

	public void setMaxMax(int maxMax) {
		this.maxMax = maxMax;
	}

	public int getMinMax() {
		return minMax;
	}

	public void setMinMax(int minMax) {
		this.minMax = minMax;
	}
}
