package jb.noisegenerator.instrument;

import java.util.Random;

public class TimeBetweenNotesManager extends ChangingDataManager {
	private double average;
	private double averageTimeBetweenNotes = 0.001;

	public TimeBetweenNotesManager(Random randomGenerator, double averageInterval, double average) {
		super(randomGenerator, averageInterval);
		this.average = average;
	}

	@Override
	void onChange() {
		averageTimeBetweenNotes = randomGenerator.nextDouble() * (average * 2) + 0.0001;
	}

	public double getAverageTimeBetweenNotes() {
		return averageTimeBetweenNotes;
	}

	public double getAverage() {
		return average;
	}

	public void setAverage(double average) {
		this.average = average;
	}
}
