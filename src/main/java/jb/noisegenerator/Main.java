package jb.noisegenerator;

import jb.noisegenerator.instrument.FreeRangeInstrument;

import javax.sound.sampled.*;
import java.time.Duration;

public class Main {
	public static void main(String[] args) {
		int channelsCount = 2;
		int resolution = 4;
		int frameSize = channelsCount * resolution;

		Duration length = Duration.ofSeconds(120);
		SoundGenerator soundGenerator = new SoundGenerator();
		soundGenerator.addInstrument(new FreeRangeInstrument(128));
		soundGenerator.addInstrument(new FreeRangeInstrument(256));
		soundGenerator.addInstrument(new FreeRangeInstrument(512));
		soundGenerator.addInstrument(new FreeRangeInstrument(1024));
		soundGenerator.addInstrument(new FreeRangeInstrument(421));
		soundGenerator.addInstrument(new FreeRangeInstrument(4231));
		soundGenerator.addInstrument(new FreeRangeInstrument(42312));
		soundGenerator.addInstrument(new FreeRangeInstrument(423126));
		soundGenerator.addInstrument(new FreeRangeInstrument(423127));

		AudioFormat audioFormat = new AudioFormat(
				AudioFormat.Encoding.PCM_SIGNED,
				Constants.SAMPLE_RATE,
				32,
				channelsCount,
				frameSize,
				Constants.SAMPLE_RATE,
				true
		);

		DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
		if (!AudioSystem.isLineSupported(info)) {
			System.out.println("Could not find any audio output.");
			System.exit(1);
		}

		SourceDataLine line;
		try {
			line = (SourceDataLine) AudioSystem.getLine(info);
			line.open(audioFormat);
			line.start();

			soundGenerator.playSong(length, line);

			// wait until the entire data has been played
			line.drain();
			line.stop();
			line.close();
		} catch (LineUnavailableException ex) {
			System.out.println("Could not open audio output.");
			System.exit(1);
		}
	}
}
