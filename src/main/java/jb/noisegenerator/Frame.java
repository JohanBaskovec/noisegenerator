package jb.noisegenerator;

import com.google.common.math.DoubleMath;

public class Frame {
	private int leftChannel;
	private int rightChannel;

	public Frame() {
	}

	public Frame(int leftChannel, int rightChannel) {
		this.leftChannel = leftChannel;
		this.rightChannel = rightChannel;
	}

	public void applyVolume(double volume) {
		leftChannel = (int) SoundMaths.multiplyWithOverflow(leftChannel, volume);
		rightChannel = (int) SoundMaths.multiplyWithOverflow(rightChannel, volume);
	}

	public void applyPanning(Panning panning) {
		this.leftChannel *= panning.getLeftVolume();
		this.rightChannel *= panning.getRightVolume();
	}

	public void add(Frame other) {
		this.leftChannel += other.leftChannel;
		this.rightChannel += other.rightChannel;
	}

	public int getLeftChannel() {
		return leftChannel;
	}

	public void setLeftChannel(int leftChannel) {
		this.leftChannel = leftChannel;
	}

	public int getRightChannel() {
		return rightChannel;
	}

	public void setRightChannel(int rightChannel) {
		this.rightChannel = rightChannel;
	}
}
