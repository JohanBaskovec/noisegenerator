package jb.noisegenerator.note;

import jb.noisegenerator.Frame;

public class ConstantNote extends Note {
	private final int value;

	public ConstantNote(double duration, int value) {
		super(duration);
		this.value = value;
	}

	@Override
	public Frame getFrame() {
		age += 1.0 / 44100;
		return new Frame(value, value);
	}

	@Override
	protected double getTargetFrequency() {
		return 0;
	}

	@Override
	public void setTargetFrequency(double frequency) {
		// constant note frequency is always 0
	}
}
