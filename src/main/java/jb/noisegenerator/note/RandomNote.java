package jb.noisegenerator.note;

import jb.noisegenerator.Constants;
import jb.noisegenerator.Frame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomNote extends Note {
	private final Random randomGenerator;
	private List<NotePart> parts = new ArrayList<>();
	private List<NotePart> repeatingParts = new ArrayList<>();
	private double volume = 0.0;
	private double attackDuration;
	private double decayDuration;
	private double decayThreshold;
	private boolean repeating = false;
	private double repeatingTimer;
	private double repeatingLength;
	private int repeatTimes;

	public RandomNote(
			double duration,
			double attackDuration,
			double decayDuration,
			Random randomGenerator,
			double baseFrequency
	) {
		super(duration);
		this.randomGenerator = randomGenerator;
		this.attackDuration = attackDuration;
		this.decayDuration = decayDuration;
		decayThreshold = duration - decayDuration;
		this.baseFrequency = baseFrequency;

		int nParts = randomGenerator.nextInt(20) + 1;
		double totalSpace = 0;
		double availableVolume = 1.0;
		double sweepDuration = duration * randomGenerator.nextDouble();
		for (int i = 0; i < nParts; i++) {
			double space = randomGenerator.nextDouble() * (baseFrequency / 10.0);
			totalSpace += space;
			double volume = availableVolume * randomGenerator.nextDouble();
			addPart(new NotePart(baseFrequency + totalSpace, volume, sweepDuration));
			availableVolume -= volume;
			if (availableVolume <= 0) {
				break;
			}
		}
		if (availableVolume > 0) {
			getParts().get(0).increaseVolume(availableVolume);
		}
	}

	public void addPart(NotePart part) {
		this.parts.add(part);
	}

	@Override
	public Frame getFrame() {
		if (age < attackDuration) {
			volume = age / attackDuration;
		} else if (age > decayThreshold) {
			double timeLeft = duration - age;
			volume = timeLeft / decayDuration;
		} else {
			volume = 1.0;
		}
		if (randomGenerator.nextInt(Constants.SAMPLE_RATE * 2) == 0 && !repeating) {
			repeatingTimer = 0;
			repeatingParts.clear();
			repeatTimes = 0;
			// repeat at least twice during the remaining time
			repeatingLength = (randomGenerator.nextDouble() * (duration - age)) / 2;
			for (NotePart part: parts) {
				try {
					repeatingParts.add((NotePart) part.clone());
				} catch (CloneNotSupportedException e) {
					e.printStackTrace();
				}
			}
			repeating = true;
		}
		if (repeating) {
			if (repeatingTimer > repeatingLength) {
				repeatTimes++;
				parts.clear();
				for (NotePart part: repeatingParts) {
					try {
						parts.add((NotePart) part.clone());
					} catch (CloneNotSupportedException e) {
						e.printStackTrace();
					}
				}
				repeatingTimer = 0;
			}
			repeatingTimer += 1.0 / 44100;
		}
		Frame finalFrame = new Frame();
		for (NotePart part: parts) {
			finalFrame.add(part.getFrame());
		}
		finalFrame.applyVolume(volume);
		age += 1.0 / 44100;

		return finalFrame;
	}

	@Override
	public double getTargetFrequency() {
		return targetFrequency;
	}

	@Override
	public void setTargetFrequency(double targetFrequency) {
		// the space between each part stays constant
		double diff = targetFrequency - parts.get(0).getInstantFrequency();
		for (NotePart notePart: parts) {
			notePart.setTargetFrequency(notePart.getInstantFrequency() + diff);
		}
		this.targetFrequency = targetFrequency;
	}

	public List<NotePart> getParts() {
		return parts;
	}

	public void setParts(List<NotePart> parts) {
		this.parts = parts;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getAge() {
		return age;
	}

	public void setAge(double age) {
		this.age = age;
	}

	public double getDuration() {
		return duration;
	}

	public void setDuration(double duration) {
		this.duration = duration;
	}
}
