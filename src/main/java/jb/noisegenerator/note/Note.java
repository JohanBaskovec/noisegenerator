package jb.noisegenerator.note;

import jb.noisegenerator.Frame;

public abstract class Note {
	protected double age = 0;
	protected double duration;
	protected double baseFrequency;
	protected double targetFrequency = 50;

	protected Note(double duration) {
		this.duration = duration;
	}

	public abstract Frame getFrame();
	abstract protected double getTargetFrequency();
	public abstract void setTargetFrequency(double frequency);

	public boolean isExpired() {
		return age >= duration;
	}
}
