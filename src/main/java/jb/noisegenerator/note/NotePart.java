package jb.noisegenerator.note;

import com.google.common.math.DoubleMath;
import jb.noisegenerator.Constants;
import jb.noisegenerator.Frame;

public class NotePart implements Cloneable {
	private double volume = 1.0;
	private double frequency = 50;
	private double baseFrequency;
	private double targetFrequency;
	private double phaseAccumulator;
	private double instantFrequency;
	private double delta;
	private double fDelta;
	private int sampleRate = 44100;
	public static final int maxValue = Integer.MAX_VALUE;
	private double sweepDuration;

	public NotePart(double frequency, double volume, double sweepDuration) {
		baseFrequency = frequency;
		instantFrequency = frequency;
		targetFrequency = frequency;
		this.volume = volume;
		this.sweepDuration = sweepDuration;
	}

	public Frame getFrame() {
		Frame frame = new Frame(
				createSample(),
				createSample()
		);
		phaseAccumulator += delta;
		if (!DoubleMath.fuzzyEquals(instantFrequency, targetFrequency, 0.001)) {
			instantFrequency += fDelta;
		}
		delta = Constants.TAU * instantFrequency / sampleRate;
		return frame;
	}

	public int createSample() {
		return (int) (Math.sin(phaseAccumulator) * (maxValue * volume));
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getFrequency() {
		return frequency;
	}

	public void setFrequency(double frequency) {
		this.frequency = frequency;
	}

	public double getTargetFrequency() {
		return targetFrequency;
	}

	public void setTargetFrequency(double targetFrequency) {
		this.targetFrequency = targetFrequency;
		baseFrequency = instantFrequency;
		delta = Constants.TAU * baseFrequency / sampleRate;
		if (sweepDuration == 0) {
			fDelta = 0;
		} else {
			fDelta = (targetFrequency - baseFrequency) / (sampleRate * sweepDuration);
		}
	}

	public double getPhaseAccumulator() {
		return phaseAccumulator;
	}

	public void setPhaseAccumulator(double phaseAccumulator) {
		this.phaseAccumulator = phaseAccumulator;
	}

	public double getInstantFrequency() {
		return instantFrequency;
	}

	public void setInstantFrequency(double instantFrequency) {
		this.instantFrequency = instantFrequency;
	}

	public double getDelta() {
		return delta;
	}

	public void setDelta(double delta) {
		this.delta = delta;
	}

	public int getSampleRate() {
		return sampleRate;
	}

	public void setSampleRate(int sampleRate) {
		this.sampleRate = sampleRate;
	}

	public void increaseVolume(double availableVolume) {
		volume += availableVolume;
	}
}
